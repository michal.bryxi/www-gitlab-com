---
layout: job_page
title: "Content Marketing"
---

## Content Marketing Associate

GitLab is looking for a highly creative, self-motivated content marketer to help drive awareness and interest in GitLab. The Content Marketing Associate position is responsible for understanding and engaging our target audiences across a variety of channels and content formats and owning content creation from start to finish.

A successful Content Marketing Associate has strong empathy and respect for the developer experience. They are able to put themselves in the shoes of others in order to understand their needs, pain points, preferences, and ambitions and translate that into multimedia experiences. Additionally, they are excited about experimentation and analyzing the results.

### Responsibilities

- Project manage content development from start to finish
- Produce and update high-quality and engaging content including (but not limited to) blog posts, webinars, emails, white papers, web pages, and case studies
- Work with product marketing to develop a deep understanding of our product messaging and translate them into engaging narratives and multimedia experiences
- Work with internal subject matter experts and thought leaders to amplify their reach and influence
- Assist in building formalized content operations processes

### Requirements

- Excellent writer and researcher with a strong ability to grasp new concepts quickly
- Degree in marketing, journalism, communications or related field
- Strong communication skills without a fear of over communication. This role will require effective collaboration and coordination across internal and external stakeholders
- Ability to empathize with the needs and experiences of developers
- Extremely detail-oriented and organized, able to meet deadlines
- You share our [values](/handbook/values/), and work in accordance with those values
- BONUS: A passion and strong understanding of the industry and our mission

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team/).

- Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Candidates will complete a take-home exercise writing a 300-500 word blog post from a provided prompt. Send the completed exercise to our Content Marketing Manager.
- Next, candidates will be invited to schedule a series of 45 minute interviews with our Content Marketing Manager, Content Editor, and Senior Product Marketing Manager
- Candidates will then be invited to schedule 45 minute interviews with our Senior Director of Marketing and Sales Development and our CMO.
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).

## Specialties

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab here.

## Cloud Native 

The Cloud Native Content Associate will work with the Director of Cloud Native Alliances to build and execute on our cloud native thought leadership strategy.  This hire will have the opportunity to learn about two organizations at close quarters - Alliances and Marketing. Career development is critical for each employee and the Cloud Native Content Associate will be enabled to pursue their goals in whichever organization they want to grow into for the long term.

## Responsibilities
- Develop Cloud Native Event & CFP Calendar keeping Marketing in the loop
- Create and Project Manage a Cloud Native Editoral Calendar
- Schedule and Enable Cloud Native webinars
- Represent GitLab at the CNCF Marketing Committee
- Support the Director of Cloud Native Alliances in a monthly oped contribution
- Build ROI measurement system to measure impact of activities

## Requirements
- 2 years or more experience
- Excellent project management, organization and communication skills
- Experience coordinating across multiple departments/teams
- Experience in the DevOps, Cloud Computing space
- Bonus: Experience working with CNCF
- Bonus: Some technical education, self learning or work experience

## Manager, Content Marketing

GitLab is looking for a prolific content strategist and creator to join our team. This is a highly visible and valuable position to define GitLab’s content strategy and create content that will expand our company’s digital footprint, awareness, subscribers, and leads. Our ideal candidate is a strong writer and editor, is equally creative and detailed oriented.

### Responsibilities

- Manage a small team of content and social media marketers.
- Define, implement, and regularly iterate on GitLab's content marketing strategy.
- Drive broad awareness of GitLab through thought leadership and PR efforts.
- Develop and implement a content operations process for managing content development, publishing, amplification, and measurement.
- Build and maintain a rich editorial calendar to support marketing campaigns, SEO, events, product releases, and company announcements.
- Collaborate with product marketing to communicate our positioning and messaging to new audiences.
- Grow and maintain our newsletter and webcast subscriber lists.
- Oversee our webcast, blog, and social media programs.
- Collaborate with sales team, advocates, influencers, industry experts to generate captivating use cases and thought leadership content.
- Perform regular content and gap analysis to analyze which content and approaches are working and why.

### Requirements

- Degree in marketing, journalism, communications or related field
- 3-5 years experience in content marketing, social media, or journalism at a technology company, preferably an enterprise technology company
- A dual-minded approach: Highly creative and an excellent writer/editor but can also be process-driven, think scale, and rely on data to make decisions.
- Strong communication skills without a fear of over communication. This role will require effective collaboration and coordination across internal and external contributors.
- Extremely detail-oriented and organized, able to meet deadlines
- Obsessive about content quality not quantity
- Regular reporting on how content and channel performance to help optimize our content marketing efforts
- You share our [values](/handbook/values), and work in accordance with those values.
- BONUS: A passion and strong understanding of the industry and our mission.

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team/).

- Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a series of 45 minute interviews with our Content Marketing Manager, Content Editor, and Senior Product Marketing Manager
- Candidates will then be invited to schedule 45 minute interviews with our Senior Director of Marketing and Sales Development and our CMO.
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
