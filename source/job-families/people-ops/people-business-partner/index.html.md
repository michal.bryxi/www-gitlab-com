---
layout: job_page
title: "People Business Partner"
---

## Responsibilities

The People Business Parter (PBP) position is responsible for aligning business objectives with employees and management in designated business units. In many companies, the role is referred to as HRBP.  The position serves as a consultant to management on human resource-related issues. The successful HRBP acts as an employee champion and change agent. The position formulates partnerships across the People Ops function to deliver value-added service to management and employees that reflects the business objectives of the organization. The position will include international human resource responsibilities. The HRBP maintains an effective level of business literacy about the business unit's financial position, its midrange plans, its culture and its competition.

In addition, this position will work with the People Ops Team and Management to deliver great training and development opportunities to the work force, keeping the learning and develop initiatives simple and effective. Through their partnership with the business, they will identify the most impactful and needed trainings and find way to ensure the those learnings are accessible to the organization.

### Responsibilities

* Forms effective relationships with the client groups and consults with line management, providing People guidance when appropriate.
* Analyzes trends and metrics in partnership with the People Ops group to develop solutions, programs, and opportunities for learning
* Manages and resolves complex employee relations issues. Conducts effective, thorough and objective investigations.
* Maintains in-depth knowledge of legal requirements related to day-to-day management of employees, reducing legal risks and ensuring regulatory compliance. Partners with the legal department as needed/required.
* Works closely with management and employees to improve work relationships, build morale, and increase productivity and retention.
* Partners with colleagues outside the US to ensure a vibrant and effective workplace.
* Provides guidance and input on business unit restructures, workforce planning and succession planning.
* Identifies training needs for business units and individual executive coaching needs.
* Participates in evaluation and monitoring of training programs to ensure success. Follows up to ensure training objectives are met.

## Requirements

* Strong attention to detail and ability to work well with changing information
* Comfortable using technology.
* Effective and concise verbal and written communication skills with the ability to collaborate with cross functional team members
* A team player who acts with a sense of urgency and adapts to a fast-paced and ever-changing environment
* Resourceful and takes initiative to seek internal and external resources when problem solving
* Have implemented various learning and development programs that are aligned to the business needs
* 4-7+ years experience as an HRBP, with 2 years of experience with Learning and Development programs
* Extensive experience in global Sales & Marketing compensation plans.
* Experience working with Global Talent in areas like Europe, India, and China
* Very strong EQ, with fine tuned instincts and problem solving skills.
