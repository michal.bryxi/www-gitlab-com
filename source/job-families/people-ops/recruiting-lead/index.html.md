---
layout: job_page
title: "Recruiting Lead, Sales & Marketing"
---

## Responsibilities

- Own a req load with a primary focus on senior level/executive roles within Marketing & Sales
- Collaborate with managers to understand requirements and establish effective recruiting strategies
- Identify creative and strategic ways to source great people
- Assess candidate interest and ability to thrive in an open source culture
- You will lead a collaborative remote based Marketing & Sales recruiting team that can scale to the dynamic demands of a rapidly growing world-wide technology company
- Provide an exceptional and high touch candidate experience
- Mentor, guide, and grow the careers of all team members
- Help define consistent data-driven hiring metrics and goals
- Create and execute innovative sourcing strategies and recruiting campaigns
- Act as a key business partner to members of the Marketing & Sales organizations to improve processes for recruiting sales & marketing professionals
- Work closely with various internal Marketing & Sales groups to understand business requirements, and consult on talent solutions
- Stay connected to the competitive landscape, including trends in Marketing & Sales recruiting and compensation
- Ensure a that you and your team maintain a high level of data integrity with our ATS and other People systems

## Requirements
- You have at least 6 years of recruiting experience within a growing organization
- You have at least 2 years experience recruiting for executive level roles within Marketing & Sales
- Consistent track record with sourcing, recruiting, and closing extraordinary talent (especially passive)
- You have worked closely with sourcers, recruiters, and coordinators
- Experience working directly with Marketing & Sales managers
- High sense of urgency
- Proven organizational skills with high attention to detail and the ability to prioritize
- Confidence to learn new technologies (MAC, Google Suite, GitLab) and translate that learning to others
- Willingness to learn and use software tools including Git and GitLab
- Experience building and defining recruiting pipeline metrics and data
- Experience and proficiency with Applicant Tracking Systems and other recruiting software (ideally including Greenhouse and LinkedIn Recruiter)
- Successful completion of a [background check](https://gitlab.com/gitlab-com/www-gitlab-com/master/handbook/people-operations/code-of-conduct#background-checks)
- You share our values, and work in accordance with those [values](https://gitlab.com/gitlab-com/www-gitlab-com/master/handbook/values)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our team page.

- Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters
- Next, candidates willbe invited to schedule a 30 minute interview with one of our Senior Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Recruiting Director
- After that, candidates will be invited to schedule a 30 minute interview with our Chief Culture Officer
- Next, the candidate will be invited to interview with a Leader on the Sales & Marketing team
- Finally, our CEO may choose to conduct a final interview

Additional details about our process can be found on our [hiring page](/handbook/hiring).
