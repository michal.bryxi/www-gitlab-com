---
layout: markdown_page
title: Is it any good?
suppress_header: true
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

More than 100,000 organizations and millions of users depend on GitLab for their software development and operations.
[Almost 2000 people contributed code to GitLab](http://contributors.gitlab.com/) (See our 2011 - 2018 [Feature Growth Chart](https://about.gitlab.com/2018/08/16/gitlab-ranked-44-on-inc-5000-list/)) and we've [liked thousands of tweets](https://twitter.com/gitlab/likes) from [our 50,000 followers](https://twitter.com/gitlab/followers).
This rest of this page contains some of the external acknowledgement GitLab has received.

## GitLab Has 2/3 Market Share in the Self-Managed Git Market

With more than 100,000 organizations self-hosting GitLab, we have the largest share of companies who choose to host their own code. We’re estimated to have two-thirds of the single tenant market. When [Bitrise surveyed](http://blog.bitrise.io/2017/01/27/state-of-app-development-in-2016.html#self-managed) ten thousand developers who build apps regularly on their platform, they found that 67 percent of self-managed apps prefer GitLab’s on-premise solution.

![Image via Bitrise blog](/images/blogimages/bitrise-self-hosted-chart.png){: .shadow}<br>

Similarly, in their survey of roughly one thousand development teams, [BuddyBuild found](https://www.buddybuild.com/blog/source-code-hosting#selfhosted) that 79% of mobile developers who host their own code have chosen GitLab:

![Image via buddybuild blog](/images/blogimages/buddybuild-self-hosted-chart.png){: .shadow}<br>

In their articles, both Bitrise and BuddyBuild note that few organizations use self-managed instances. We think there is a selection effect since both of them are SaaS-only offerings.

Based on our experience, the vast majority of enterprises ([organizations with over 5000 IT + TEDD employees](/handbook/sales/#market-segmentation)) self host their source code server (frequently on a cloud service like AWS or GCP) instead of using a SaaS service.

Another assumption is that git is the most popular version control technology for the enterprise. Certainly not every enterprise has switched (completely) yet but it does seem that git is [almost 10 times larger than SVN and Mercurial](https://trends.google.com/trends/explore?q=git,svn,perforce,mercurial,tfs).

## GitLab CI is the fastest growing CI/CD solution

Our commitment to seamless integration extends to CI. Integrated CI/CD is both more time and resource efficient than a set of distinct tools, and allows developers greater control over their build pipeline, so they can spot issues early and address them at a relatively low cost. Tighter integration between different stages of the development process makes it easier to cross-reference code, tests, and deployments while discussing them, allowing you to see the full context and iterate much more rapidly. We've heard from customers like [Ticketmaster](/2017/06/07/continous-integration-ticketmaster/) that adopting GitLab CI can transform the entire software development lifecycle (SDLC), in their case helping the Ticketmaster mobile development team deliver on the longstanding goal of weekly releases. As more and more companies look to embrace CI as part of their development methodology, having CI fully integrated into their overall SDLC solution will ensure these companies are able to realize the full potential of CI. You can read more about the benefits of integrated CI in our white paper, [Scaling Continuous Integration](http://get.gitlab.com/scaled-ci-cd/).

In his post on [building Heroku CI](https://blog.heroku.com/building-tools-for-developers-heroku-ci), Heroku’s Ike DeLorenzo noted that GitLab CI is “clearly the biggest mover in activity on Stack Overflow,” with more popularity than both Travis CI and Circle CI.

GitLab is the second most popular CI system in [a report on cloud trends from Digital Ocean](https://assets.digitalocean.com/currents-report/DigitalOcean-Currents-Q4-2017.pdf).

![Image via Digital Ocean](/images/ci/gitlab-popular-ci.png){: .shadow}

## GitLab CI is a leader in the The Forrester Wave™

![Forrester Wave graphic](/images/blogimages/forrester-ci-wave-graphic.png){: .shadow}

[ Forrester has evaluated GitLab as a Leader in Continuous Integration in The Forrester Wave™: Continuous Integration Tools, Q3 2017 report.](/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/)

## GitLab is one of the top 30 open source projects

[GitLab is one of the 30 Highest Velocity Open Source Projects](/2017/07/06/gitlab-top-30-highest-velocity-open-source/).

## GitLab has more than 1800 contributors

[GitLab contributors list](http://contributors.gitlab.com/).

## Why this page called is called 'is it any good?'

When people first hear about a new product they frequently ask if it is any good. A Hacker News user [remarked](https://news.ycombinator.com/item?id=3067434): 'Note to self: Starting immediately, all raganwald projects will have a “Is it any good?” section in the readme, and the answer shall be “yes."'. We took inspiration from that and added it to the [GitLab readme](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/README.md#is-it-any-good). This page continues that tradition.
