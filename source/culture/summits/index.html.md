---
layout: markdown_page
title: "GitLab Summits"
---

- TOC
{:toc}
- [Summit From a Leadership Perspective](/culture/summits/summit-training)


## What, where, and when
{: #what-when-where}

We try to get together every 9 months or so to get face-time, build community,
and get some work done! Since our team is scattered all over the globe, we try to
plan a different location for each Summit.

## Goal

The goal of our Summits is to **get to know the people in the GitLab community better**.
The better you know people the easier it is to collaborate.
We want to build trust between groups.

## Who
{: #who}

All [GitLab Team](/team/), their significant others, and the [Core Team](/core-team/).

## Summits

### Upcoming Summit

The next summit coming up will be from August 23rd to 29th 2018 in Cape Town, South Africa!
All info to prep for this in on the [project page](https://gitlab.com/summits/2018-Summit/blob/master/urgent-important-info.md)


#### Countdown

<p class="alert alert-gitlab-purple text-center">
<i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i>
Counting down to August 23rd, 2018!
<i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i>
<br/><br/>
<span class="h3" id="nextSummitCountdown">
<i class="fas fa-spinner fa-spin fa-fw" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>
</span>
</p>

{: #previous-summits}

### Summit in Crete, Greece

By October 2017 we had 200 team members and 65 significant others getting together in Greece to enjoy the beautiful islands of Crete and Santorini.

![GitLab Team - Greece - 2017](/images/gitlab_team_summit_greece_2017.png)

### Summit in Cancun, Mexico

In January 2017 we met in Cancun, Mexico, where roughly 150 team members and 50
significant others flew in from 35 different countries.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/XDfTj8iv9qw" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<br>

### Summit in Austin, TX, the USA

In May 2016, our team of 85 met up in Austin, TX to see if they were (still) as
awesome as seen on Google Hangout.

Here's some footage our team put together to show how much fun we had.

<figure class="video_container">
  <iframe src="https://player.vimeo.com/video/175270564" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<br>

### Summit in Amsterdam, the Netherlands

Here are some impressions from our [first Summit](/2015/11/30/gitlab-summit-2015/) in October 2015.

<br>

## Leisure time around the Summit
* The GitLab Summit is a work trip, not an incentive trip
* If you want to enjoy the resort facilities or the area around it, feel free to book an extra day or more _before or after_ the summit
* The Summit organization will plan "regular work time" for you to do regular work such as handling emails
* When you sign up for the activities we plan for the non-work days, you agree to show up.
* If you don't show up for the activity, you will be responsible for the costs involved for the seat you give up after the RSVP deadline has passed.
* When you replied "Maybe" or didn't reply at all, you understand that there will not be a ticket booked for you and you won't be able to join the activitie(s).

## Bring your significant other

* Significant Others (SO) are very welcome to attend
* One SO per team member
* You are responsible for the SO you invite
* Your SOs presence should not distract you from engaging with other team members and actively participating in Summit activities
* SOs should do their best to get to know many people
* SOs are welcome at all presentations, events, and meetings that are open to
all team members
* If you're having a meal with your SO, pick a table with more than two seats
so you can invite others to join you

## Having your children join you

Children are strongly discouraged from attending. We have observed from past summits
that contributors who have chosen to bring children spend significantly less time
collaborating with GitLab coworkers. Accordingly, we are requesting that if you
must travel with family members you fully engage in as many group activities as
you are physically able, invite team members to join you during meals, attend all
company meetings, and recognize that the Summit is an investment in the continued
growth of the organization.

## Behavior

* Attending the summit is optional but recommended. Most people report it is great to get to know each-other better and the schedule is fun, about 90% of the team is able to attend.
* The executive team is required to attend for the full duration of the summit.
* Wear your name-tag when you're outside your room, including during excursions, meals, and on departure day.
* Try to join different people every time we sit down for a meal
* Try to form a personal bond with team members of other teams.
* The summit is great for informal meetings and brainstorming, like User Generated Content discussions. People already know their team, so try to make UGC sessions cross functional.
* Don't plan meetings and 1-1's with your own team at the summit, we already do these when we're not at the summit. It is OK to organize one dinner with the team.
* Prior to the summit, ensure to communicate to any external stakeholders (i.e. candidates, customers, vendors, etc) that response time may be less reliable, as you will be out of the office and will not have as much access to email and calls.
* Respect the laws and customs of the location we are visiting.

## Health and safety
{: #health-and-safety}

* Look after your self during the summit and avoid summit burnout.  It can be an exciting time with lots of new people to meet and things going on you want to take part in.  Remember to take down-time if you need it to recuperate during the week rather than trying to [burn the candle at both ends](https://dictionary.cambridge.org/dictionary/english/burn-the-candle-at-both-ends) and risking exhaustion.
* Every year about a third of us have some kind of flu after the summit, so please take infection prevention seriously.
* Use fist-bumps instead of handshaking to [reduce the number of people who get sick](https://www.health.harvard.edu/blog/fist-bump-better-handshake-cleanliness-201407297305).
* Use hand sanitizer after getting your food and before eating. [Shared buffet utensils spread disease.](http://www.cruisereport.com/crBlogDetail.aspx?id=3683) We will try to provide hand sanitizer.
* If you are sick please wear a [surgical mask](https://www.amazon.com/Maryger-Disposable-Procedure-Surgical-Counts/dp/B06XVMT3ZH/ref=sr_1_1_sspa?s=hpc&ie=UTF8&qid=1509481716&sr=1-1-spons&keywords=surgical+mask&psc=1) which [reduces the spreading by up to 80%](https://www.healthline.com/health/cold-flu/mask). We'll try to provide them.
* Remember our [values](/handbook/values/) and specifically the [permission to play](/handbook/values/#permission-to-play) behavior
* Be respectful of other hotel guests (e.g. don't talk on your floor when returning
to your room at night & keep your volume down at restaurants/bars).
* Utilize the resources available to understand the safety and crime considerations in the location we are visiting. Examples are the [UK's Foreign Travel Site](https://www.gov.uk/foreign-travel-advice) and the [U.S. State Department](https://travel.state.gov/content/passports/en/country.html).  If you are alarmed by what you are reading, please feel free to reach out to People Ops Team with your concerns.  We also advise reviewing the data for countries you feel are safe. You may find that even the safest countries have warnings on crime and safety.  Staying with a group and away from known dangerous areas is the most basic way of avoiding problems.

## WiFi

Great WiFi is essential to the success of the summit.
We can't have everyone in one location and not have excellent internet.

1. Main room needs one access point for every 40 people attending in the main room and management, we have our own equipment but need to buy more.
2. We need two different uplinks from two different providers.
3. We need our own router between the uplinks and the WiFi.
4. We need wired connections to the WiFi access points.
5. We need to have WiFi working before the executive team arrives.
6. We need very reliable fast connections if we are livestreaming during the summit.
7. We need a map of the property with all wiring and access points drawn in 3 months before the summit starts.

## Logistical basics

* Ensure there are large meeting rooms for team members to join work hours and presentations
* Tip: Label your charger, or other belongings, with your name for easy return to owner in case you lose it

## Presentations
{: #presentations}

The following should be assigned and/or arranged a month before the Summit:

* All interviews, presentations and content production
* Who will be presenting and when & where they will be presenting
* Projectors, wireless (non-hand-held) microphones, and any other (audio) needs
* Recording equipment such as stage cam, audience cam, presentation feed etc.
* An audio feed that goes directly from microphone into the recording
* A program and manager for live streaming
* The blog text for the presentation, including relevant materials shared after
the presentation, as well as approval and a plan to publish the blog 24 hours after the presentation is given

## User Generated Content sessions

User Generated Content sessions are 50 minute discussions on a topic that is important to you.
Topics can be on business, markets, investing, problems, opportunities, life, parenting, etc. Whatever is interesting to our team or the community.
Here's an overview of how this works during our summits.
* We request everyone to [send in topics](https://docs.google.com/spreadsheets/d/15irgt0kOlCNzltsBzWRbJoQzLFjOlOYKgREQ_PRGiuQ/edit#gid=0) to discuss during the sessions at the summit a few weeks out.
    * If you're suggesting a topic, we ask you to be the topic leader. This means the following:
    * You start the session with a short 3 minute introduction, no preparation or presentation needed.
    * During the discussion you facilitate the conversation, meaning keeping it on topic, making sure everyone is heard, and asking relevant questions.
* After all topics are received, we send a [survey](https://docs.google.com/forms/d/e/1FAIpQLScvxsf00cShgj9KIjgIx8Jj60uVFhE_WbtYbHkoa3d2-H7IxQ/viewform) to all attendees asking them to vote on the topics most interesting to them.
* Once we've received all votes, after the deadline, we select the topics for the sessions and schedule everyone in according to their preferences.
* We schedule 2, 4 hour blocks on separate days to have the sessions.
* Within each 4 hour block we schedule 4 session blocks (with multiple topics in different locations during each block) with a short break of ~10 minutes in between for a quick drink/snack or bathroom break.

## Live stream

1. During the summit we'll have a live stream that is interactive (viewers ask questions).
1. The live stream is a way to generate tangible hiring and marketing benefits, this will help to sustain the large discretionary expense of the summit.
1. There will be only one mobile camera crew and it will be easy to recognize.
1. With the mobile camera crew will be two GitLab team members, a moderator for the chat and a facilitator that will interact with other team members.
1. We want the viewers (our team members that couldn't make it, the wider community, friends and family) to feel like participants instead of an audience. From time to time the facilitator will seek interaction by talking with team members.
1. You can always decline to enter in a conversation with the facilitator, just like you can decline a conversation with another team member.
1. If you don't want to be approached by the facilitator under any circumstances you can ask for an identifier from the organization.
1. The facilitator will avoid taking to team members wearing the identifier, significant others, and children. Significant others that want to interact are very welcome to approach the facilitator themselves.
1. Team members wearing the identifier and significant others might be visible in the shot as passers-by's. The camera crew try to avoid children passers-by's. As said elsewhere on this page we strongly discourage children from attending.

<script>
var nextSummitDate = new Date("Aug 23, 2018 08:00:00").getTime();

var x = setInterval(function() {
    var now = new Date().getTime();

    var distance = nextSummitDate - now;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
    document.getElementById("nextSummitCountdown").innerHTML = days + " days " + hours + " hours "
    + minutes + " minutes " + seconds + " seconds ";

    // If the count down is over, write some text
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("nextSummitCountdown").innerHTML = "Already happened!";
    }
}, 1000);
</script>
