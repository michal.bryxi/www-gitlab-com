- url: auto-devops
  youtube_url: https://www.youtube.com/embed/6LZQCCVGVDg
  title: "Release Radar: Auto DevOps"
  subtitle: "Accelerate delivery by 200% in two steps"
  date: June 27 9am PT / 4pm UTC
  form: 1592
  image: /images/concurrent-devops/concurrent-devops.svg
  content:
    - "How many steps does it take to go from code to production? Automated pipelines are supposed to make software delivery faster and more efficient, but often require many integrations that need to be managed and maintained—making the process not so automatic."
    - "However, with Auto DevOps, you can go from code to production in just two steps. Your pipeline is built into the same application as your repository, no integration necessary. Just write and commit your code, and Auto DevOps will do the rest: detect the language of your code and automatically build, test, measure code quality, scan for security issues, package, monitor, and deploy the application. Auto DevOps removes the barriers to shipping secure, bug-free code, fast."
    - "Join us for a live broadcast on June 27 to learn how Auto DevOps simplifies your deployment pipeline to accelerate delivery by 200%, improves responsiveness, and closes the feedback gap between you and your users."
  what_we_cover:
    - "The competitive edge imperative: speed, feedback, and responsiveness."
    - "What is Auto Devops and how does it work?"
    - "2-step demo and pipeline interface walk through."

- url: devops-speed-to-mission
  title: "DevOps: Powering your Speed to Mission"
  subtitle: "A panel discussion on DevOps in the Public Sector"
  date: September 18th, 12pm ET / 4pm UTC
  form: 1592
  image: /images/webcast/public-sector.svg
  content:
    - "Interested in learning what powerful impact DevOps is having on the Federal government and how DevOps can power your speed to mission? Join us for a webcast on Tuesday, September 18th at Noon Eastern to learn more."
    - "Join Tom Suder, President and Founder of ATARC, Leo Garciga, Chief of JD-OI6 and Chief Technology Officer for the JIDO/DTRA, Rob Brown, Division Chief and Senior Solutions Architect with Infrastructure Enterprise (EID) at USCIS, and John Jeremiah, Subject Matter Expert at GitLab for an interactive discussion."
    - "We will be taking questions at the end of the discussion, so come prepared to ask tough questions to the panelists."
  what_we_cover:
    - "Cultural challenges faced by the government"
    - "Legal, regulatory and policy challenges faced by the government, and how to work within them"
    - "Best practices for successfully implementing a DevOps methodology"
    - "Tools and techniques which can be used within Federal agencies"
  speakers:
    - name: "Tom Suder"
      title: "ATARC, Founder & President"
      about: "A respected thought leader in the Federal IT community, Tom Suder is the Founder and President of the Advanced Technology Academic Research Center (ATARC), a non-profit organization that provides a collaborative forum for Federal government, academia and industry to resolve emerging technology challenges."
      image: /images/webcast/tom-suder.jpg
    - name: "Leo Garciga"
      title: "Chief of JD-OI6 and Chief Technology Officer"
      about: "Leo Garciga serves as the Chief of JD-OI6 and Chief Technology Officer for the Joint Improvised-Threat Defeat Organization (JIDO), Defense Threat Reduction Agency (JIDO/DTRA). In these roles, he provides leadership and oversight of Mission Information Technology services and personnel that directly contribute to the implementation of the JIDO/DTRA mission and its support to the warfighter, Department of Defense (DoD), Combatant Commanders, Coalition partners, the intelligence and interagency organizations."
      image: /images/webcast/leo-garciga.png
    - name: "Rob Brown"
      title: "Chief Architect at the U.S. Citizenship and Immigration Services (USCIS)"
      about: "Rob Brown is the Chief Architect at the U.S. Citizenship and Immigration Services (USCIS). He is the technical lead for digitizing immigration and eProcessing. Rob has 18 years of experience in technical, managerial and business development solutions for the IT industry. Prior to USCIS, Rob was a Senior DevOps Engineer with Booz Allen Hamilton, responsible for leading the design, development, and implementation of Continuous Integration (CI) processes. He was also a member of the Agile Engineering and Test Automation Services (AETAS) Center of Excellence (COE). Rob holds a MS in Bioinformatics from George Mason University and a BS in Microbio/BioChem from Virginia Tech. He holds certifications from (ISC)², Sourcefire, Fonality, IC Agile and Juniper."
      image: /images/webcast/rob-brown.png
    - name: "John Jeremiah"
      title: "Enterprise DevOps evangelist and product marketing leader at GitLab"
      about: "John is an enterprise DevOps evangelist and product marketing leader at GitLab with over 20 years of IT leadership and software development experience. John helps to guide customers to adapt and thrive in a world of rapid change and digital disruption. He has held a variety of leadership roles with the U.S. Navy, IT consulting and Fortune 500 IT organizations and his experience spans from application developer, project and program manager, and IT Director where he has led the adoption of an Agile and CMMI Maturity Level 3 process framework. He has spoken at numerous conferences and events ranging from Interop to TedX. After receiving a bachelor’s degree from Oregon State University, Jeremiah earned his MS in Information Technology from The George Washington University School of Business and served nine years as a Naval Officer with the United States Navy."
      image: /images/team/johnjeremiah-crop.jpg

- url: gary-gruver-discussion
  youtube_url: https://www.youtube.com/embed/EqdAFtTngUg
  title: "DevOps Discussion with Gary Gruver"
  subtitle: "An insightful discussion on the details of a DevOps transformation"
  date: August 2, 9am PT / 4pm UTC
  form: 1592
  image: /images/webcast/devops-pipeline-bug.svg
  content:
    - "Deciding to start a DevOps transformation is the first step in eliminating inefficiencies, but how do teams proceed?"
    - "Join Gary Gruver, author of Starting and Scaling DevOps in the Enterprise, as he shares his experience consulting with organizations to successfully adopt a DevOps model. He’ll offer advice on the influencers who should be involved in discussions and pinpoint the areas where a transformation should begin."
  what_we_cover:
    - "The most common DevOps concerns"
    - "How enterprises can get started with DevOps"
    - "Best practices for successfully implementing a DevOps methodology"
    - "Key players in the transformation"
  speakers:
    - name: "Gary Gruver"
      title: <a href="https://garygruver.com/" target="_blank">garygruver.com</a>
      about: "Gary Gruver is a consultant that spends his time running workshops with large organizations to help them transform their software development and delivery processes. He is the author of Starting and Scaling DevOps in the Enterprise and co-author of two other books: Leading the Transformation: Applying Agile and DevOps principles at Scale and A Practical Approach to Large Scale Agile Development: How HP Transformed LaserJet FutureSmart Firmware. He is also an experienced executive with a proven track record of transforming software development and delivery processes in large organizations. First as the R&D director of the LaserJet FW group that completely transformed how they developed embedded FW. Then as VP of QA, Release, and Operations at Macy’s.com leading the journey toward Continuous Delivery."
      image: /images/webcast/gary-gruver.jpg

- url: scalable-app-deploy
  youtube_url: https://www.youtube.com/embed/uWC2QKv15mk
  title: "Scalable app deployment"
  subtitle: "with GitLab and Google Cloud Platform"
  date: April 26th, 2 PM EDT
  form: 1554
  image: /images/webcast/gitlab-gke-integration.svg
  content:
    - "With the GitLab + Google Kubernetes Engine (GKE) integration, developers have the power to spin up a Kubernetes cluster managed by Google Cloud Platform (GCP) in a few clicks. The integration’s versatility speeds up software development and delivery while maintaining security and scale, allowing developers to focus on building apps instead of managing infrastructure."
    - "Join William Chia, Senior Product Marketing Manager at GitLab, and William Denniss, Product Manager at Google, as they explain how to deploy applications at scale using GKE and GitLab’s robust Auto DevOps capabilities."
  what_we_cover:
    - "The basics of cloud environments, including benefits and challenges"
    - "How the GitLab GKE integration simplifies setting up and deploying to a Kubernetes cluster"
    - "A demo highlighting how easy it is to set up a Kubernetes Cluster, how to deploy your app using GitLab CI/CD, and how GKE allows you to deploy at scale"

- url: starting-scaling-devops
  youtube_url: https://www.youtube.com/embed/aWlJGFkRcOA
  title: "Starting and Scaling DevOps"
  subtitle: "A tactical framework for implementing DevOps principles"
  date: June 19, 9am PT / 4pm UTC
  form: 1592
  image: /images/webcast/devops-pipeline-bug.svg
  content:
    - "Companies worldwide are excited about DevOps and the many potential benefits of embarking on a DevOps transformation. The challenge, however, is figuring out where to begin and how to scale DevOps practices over time."
    - "Join Gary Gruver, author of Starting and Scaling DevOps in the Enterprise, as he covers how to analyze your current deployment pipeline to target your first improvements on the largest inefficiencies in software development and deployment. We’ll also explore the different approaches necessary for coordinating the work of small teams versus very large and complex organizations with many teams."
  what_we_cover:
    - "Aligning your DevOps transformation to business objectives"
    - "The basics of a deployment pipeline"
    - "How to collect data and identify sources of inefficiency"
    - "Applying DevOps principles"
  speakers:
    - name: "Gary Gruver"
      title: <a href="https://garygruver.com/" target="_blank">garygruver.com</a>
      about: "Gary Gruver is a consultant that spends his time running workshops with large organizations to help them transform their software development and delivery processes. He is the author of Starting and Scaling DevOps in the Enterprise and co-author of two other books: Leading the Transformation: Applying Agile and DevOps principles at Scale and A Practical Approach to Large Scale Agile Development: How HP Transformed LaserJet FutureSmart Firmware. He is also an experienced executive with a proven track record of transforming software development and delivery processes in large organizations. First as the R&D director of the LaserJet FW group that completely transformed how they developed embedded FW. Then as VP of QA, Release, and Operations at Macy’s.com leading the journey toward Continuous Delivery."
      image: /images/webcast/gary-gruver.jpg
