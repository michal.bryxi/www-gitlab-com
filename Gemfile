# If you do not have OpenSSL installed, change
# the following line to use 'http://'
source 'https://rubygems.org'

# For faster file watcher updates on Windows:
gem 'wdm', '~> 0.1.0', platforms: %i[mswin mingw x64_mingw]

# Windows does not come with time zone data
gem 'tzinfo-data', platforms: %i[mswin mingw x64_mingw jruby]

# Middleman Gems
gem 'middleman', '~> 4.2'
gem 'middleman-blog', '~> 4.0'
gem 'middleman-livereload'
gem 'middleman-minify-html'
gem 'middleman-autoprefixer'
gem 'middleman-syntax'

gem 'kramdown', '~> 1.10'
gem 'nokogiri'
gem 'sassc'
gem 'stringex'

# Replacement of therubyracer
gem 'mini_racer', '~> 0.1'

# For feed.xml.builder
gem 'builder', '~> 3.0'

# Direction generation
gem 'faraday', '>= 0.15.0'
gem 'faraday_middleware'
gem 'faraday_middleware-parse_oj', '~> 0.3'

group :development, :test do
  gem 'html-proofer'
  gem 'docopt'
  gem 'scss_lint', require: false
  gem 'rspec', '~> 3.5', require: false
  gem 'rubocop', '~> 0.50.0', require: false
end
